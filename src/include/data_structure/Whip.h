
#ifndef WHIP_H_
#define WHIP_H_

#include <queue>

#include "include/primitive/Spring.h"
#include "include/primitive/Vector3f.h"
#include "include/primitive/PointMass.h"

class Whip
{
public:
	friend std::ostream& operator<<(std::ostream& out, Whip& w)
	{
		float defLength = 0.0f;
		float currLength = 0.0f;
		for(int i = 0; i < w.linkCount; ++i)
		{
			defLength += w.links[i].length;
//			currLength += w.links[i].getCurrLength();
		}

		out << "def-length: " << defLength << std::endl;
		out << "curr-length: " << currLength << std::endl;
		out << "points (" << w.pointMassCount << ")" << std::endl;
		for(int i = 0; i < w.pointMassCount; ++i)
			out << w.pointMasses[i] << std::endl;

		out << "links(" << w.linkCount << ")" << std::endl;
		for(int i = 0; i < w.linkCount; ++i)
		{
			out << w.links[i] << std::endl;
		}

		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Whip& w)
	{
		float defLength = 0.0f;
		float currLength = 0.0f;
		for(int i = 0; i < w.linkCount; ++i)
		{
			defLength += w.links[i].length;
//			currLength += w.links[i].getCurrLength();
		}

		out << "def-length: " << defLength << std::endl;
		out << "curr-length: " << currLength << std::endl;
		out << "points (" << w.pointMassCount << ")" << std::endl;
		for(int i = 0; i < w.pointMassCount; ++i)
			out << w.pointMasses[i] << std::endl;

		out << "springs(" << w.linkCount << ")" << std::endl;
		for(int i = 0; i < w.linkCount; ++i)
		{
			out << w.links[i] << std::endl;
		}

		return out;
	}

	int timeSlice;
	int pointMassCount;
	int linkCount;
	int springCount;
	PointMass* pointMasses;
	Link* links;
	Spring* springs;

	Whip(void);
	Whip(int pointMassCount, float offset, float mass);
	virtual ~Whip(void);

	void simulate(void);
	void applyExternal(Vector3f force);
	void applyInternal(Vector3f force);
	void draw(void);
};

#endif /* WHIP_H_ */
