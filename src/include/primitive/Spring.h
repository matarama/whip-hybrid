
#ifndef SPRING_H_
#define SPRING_H_

#include <ostream>

#include "include/primitive/Vector3f.h"

class Spring
{
public:
	friend std::ostream& operator<<(std::ostream& out, Spring& s)
	{
		out << "id: " << s.id;
		out << " k: " << s.k;
		out << " def-len: " << s.defaultLength;
		out << " curr-len: " << s.startPos->calcDistance(*s.endPos);

		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Spring& s)
	{
		out << "id: " << s.id;
		out << " k: " << s.k;
		out << " def-len: " << s.defaultLength;
		out << " curr-len: " << s.startPos->calcDistance(*s.endPos);

		return out;
	}

	int id;
	float k; 				// spring constant
	float defaultLength; 	// default length of spring
	float damping; 			// damping factor
	Vector3f* startPos; 	// current position of springs head
	Vector3f* endPos; 		// current position of springs tail

	Spring(void);
	Spring(unsigned int id, float k, float defaultLength);
	Spring(unsigned int id, float k, Vector3f* startPos, Vector3f* endPos);
	virtual ~Spring(void);

	Vector3f calculateForceEnd(void);
	float getCurrLength(void);
	Vector3f getCurrLengthVector(void);
	void draw(void);
};

#endif /* SPRING_H_ */
