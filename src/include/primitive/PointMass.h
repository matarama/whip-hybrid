
#ifndef MASS_H_
#define MASS_H_

#include <vector>
#include <ostream>

#include "include/primitive/Spring.h"
#include "include/primitive/Vector3f.h"

struct PointMass
{
public:
	friend std::ostream& operator<<(std::ostream& out, PointMass& pointMass)
	{
		out << "m: " << pointMass.m;
		out << " pos(" << pointMass.position << ")";
		out << " velocity(" << pointMass.velocity << ")";
		out << " force(" << pointMass.force << ")";
		out << " fixed: " << pointMass.fixed;

		out << " attached-springs-front(" << pointMass.attachedSpringsHead.size() << "): ";
		for(int i = 0; i < pointMass.attachedSpringsHead.size(); ++i)
			out << " " << pointMass.attachedSpringsHead[i]->id;

		out << " attached-springs-back(" << pointMass.attachedSpringsTail.size() << "): ";
		for(int i = 0; i < pointMass.attachedSpringsTail.size(); ++i)
			out << " " << pointMass.attachedSpringsTail[i]->id;

		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const PointMass& pointMass)
	{
		out << "m: " << pointMass.m;
		out << " pos(" << pointMass.position << ")";
		out << " velocity(" << pointMass.velocity << ")";
		out << " force(" << pointMass.force << ")";
		out << " fixed: " << pointMass.fixed;

		out << " attached-springs-front:";
		for(int i = 0; i < pointMass.attachedSpringsHead.size(); ++i)
			out << " " << pointMass.attachedSpringsHead[i]->id;

		out << " attached-springs-back:";
		for(int i = 0; i < pointMass.attachedSpringsTail.size(); ++i)
			out << " " << pointMass.attachedSpringsTail[i]->id;

		return out;
	}

	float m;
	float dampingConstant;
	bool fixed;
	Vector3f position;
	Vector3f velocity;
	Vector3f force;
	std::vector<Spring*> attachedSpringsHead;
	std::vector<Spring*> attachedSpringsTail;

	PointMass(void);
	PointMass(float m, float dampingConstant, Vector3f position, bool fixed = false);

	void addForce(Vector3f f);
	void update(int timeSlice);
	void move(Vector3f newPosition);
	void move(int timeSlice);
	void attachHead(Spring* spr);
	void attachTail(Spring* spr);
	void draw(void);
};

#endif /* MASS_H_ */
