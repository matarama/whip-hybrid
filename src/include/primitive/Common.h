
#ifndef WHIP_COMMON_H_
#define WHIP_COMMON_H_

extern float radianToDegree(float radian);
extern float degreeToRadian(float degree);

#endif /* WHIP_COMMON_H_ */
