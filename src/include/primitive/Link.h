
#ifndef LINK_H_
#define LINK_H_

#include <ostream>

#include "include/primitive/Vector3f.h"
#include "include/primitive/PointMass.h"

struct Link
{
	friend std::ostream& operator<<(std::ostream& out, Link& link)
	{
		out << "head(" << *link.head << ") ";
		out << "tail(" << *link.tail << ") ";
		out << "length: " << link.length;

		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Link& link)
	{
		out << "head(" << *link.head << ") ";
		out << "tail(" << *link.tail << ") ";
		out << "length: " << link.length;

		return out;
	}

	PointMass* head;
	PointMass* tail;
	float length;
	Vector3f angularVelocity;
	float dampingConstant;

	Link(void);
	Link(PointMass* head, PointMass* tail, float length,
			float dampingConstant = 0.98f);
	virtual ~Link(void);

	void followThrough(void);
	void update(int timeSlice);
	void rotate(int timeSlice);

	void draw(void);
private:
	/**
	 * Since link's length is stable, any outside intervention
	 * such as velocity, force, acceleration is stripped from
	 * the linearly dependent parts (on link vector) of itself
	 * and returned.
	 */
	Vector3f calculateValidVector(Vector3f intervention);
};

#endif /* LINK_H_ */
