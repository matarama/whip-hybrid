/*
 * Vector.h
 *
 *  Created on: Nov 3, 2013
 *      Author: matara
 */

#ifndef VECTOR_H_
#define VECTOR_H_

#include <ostream>
#include <string>

struct Vector3f
{
public:
	friend std::ostream& operator<<(std::ostream& out, Vector3f& vec)
	{
		out << "x: " << vec.x << " y: " << vec.y << " z: " << vec.z;
		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Vector3f& vec)
	{
		out << "x: " << vec.x << " y: " << vec.y << " z: " << vec.z;
		return out;
	}

	float x;
	float y;
	float z;

	Vector3f(void);
	Vector3f(float x, float y, float z);
	Vector3f(const Vector3f& other);
	Vector3f& operator=(const Vector3f& other);
	virtual ~Vector3f(void);

	float dotProd(Vector3f v);
	Vector3f crossProd(Vector3f v);
	float calcDistanceSquared(Vector3f other);
	float calcDistance(Vector3f other);
	float getLengthSquared(void);
	float getLength(void);
	void normalize(void);
	float getAngle(Vector3f v);
	void reset(void);
	Vector3f getNormalized(void);
	std::string toString(void);

	Vector3f operator+(float scalar);
	Vector3f operator-(float scalar);
	Vector3f operator/(float scalar);
	Vector3f operator*(float scalar);
	Vector3f operator+(Vector3f other);
	Vector3f operator-(Vector3f other);
	Vector3f operator/(Vector3f other);
	Vector3f operator*(Vector3f other);

	Vector3f& operator+=(float scalar);
	Vector3f& operator-=(float scalar);
	Vector3f& operator/=(float scalar);
	Vector3f& operator*=(float scalar);
	Vector3f& operator+=(Vector3f other);
	Vector3f& operator-=(Vector3f other);
	Vector3f& operator/=(Vector3f other);
	Vector3f& operator*=(Vector3f other);

	bool operator==(Vector3f other);
	Vector3f operator-() const;
	bool hasOppositeDir(Vector3f other);
	bool hasSameDir(Vector3f other);

	Vector3f rotateAbout(Vector3f axis, float angle);
};

#endif /* VECTOR_H_ */
