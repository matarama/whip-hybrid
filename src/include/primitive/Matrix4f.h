
#ifndef MATRIX4F_H_
#define MATRIX4F_H_

#include <ostream>
#include "include/primitive/Vector3f.h"

struct Matrix4f
{
	friend std::ostream& operator<<(std::ostream& out, Matrix4f& m)
	{
		for(int i = 0; i < 4; ++i)
		{
			for(int j = 0; j < 4; ++j)
				out << m.data[i * 4 + j] << " ";

			out << std::endl;
		}

		return out;
	}

	friend std::ostream& operator<<(std::ostream& out, const Matrix4f& m)
	{
		for(int i = 0; i < 4; ++i)
		{
			for(int j = 0; j < 4; ++j)
				out << m.data[i * 4 + j] << " ";

			out << std::endl;
		}

		return out;
	}

	float data[16];

	Matrix4f(void);
	Matrix4f(float* data);
	Matrix4f(const Matrix4f& other);
	Matrix4f& operator=(const Matrix4f& other);
	virtual ~Matrix4f(void);

	float operator()(unsigned int i, unsigned int j) const;
	float& operator()(unsigned int i, unsigned int j);

	Matrix4f operator+(float scalar);
	Matrix4f operator-(float scalar);
	Matrix4f operator/(float scalar);
	Matrix4f operator*(float scalar);
	Matrix4f operator+(Matrix4f other);
	Matrix4f operator-(Matrix4f other);
	Matrix4f operator/(Matrix4f other);
	Matrix4f operator*(Matrix4f other);

	Matrix4f& operator+=(float scalar);
	Matrix4f& operator-=(float scalar);
	Matrix4f& operator/=(float scalar);
	Matrix4f& operator*=(float scalar);
	Matrix4f& operator+=(Matrix4f other);
	Matrix4f& operator-=(Matrix4f other);
	Matrix4f& operator/=(Matrix4f other);
	Matrix4f& operator*=(Matrix4f other);

	Vector3f operator*(Vector3f v);

private:
	void copyOther(const Matrix4f& other);
	void copy(float* data);
	void init(void);
};

#endif /* MATRIX4F_H_ */
