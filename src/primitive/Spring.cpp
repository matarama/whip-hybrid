
#include <string>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>

#include "include/primitive/Spring.h"

Spring::Spring(void)
: id(0),
  k(0.0f),
  damping(0.0f),
  defaultLength(0.0f),
  startPos(NULL),
  endPos(NULL)
{
}

Spring::Spring(unsigned int id, float k, float defaultLength)
: id(id),
  k(k),
  damping(0.0f),
  defaultLength(defaultLength),
  startPos(NULL),
  endPos(NULL)
{
}

Spring::Spring(unsigned int id, float k, Vector3f* startPos, Vector3f* endPos)
: id(id),
  k(k),
  damping(0.0f),
  defaultLength(0.0f),
  startPos(startPos),
  endPos(endPos)
{
	defaultLength = startPos->calcDistance(*endPos);
}

Spring::~Spring(void)
{
}

Vector3f Spring::calculateForceEnd(void)
{
	Vector3f dir = getCurrLengthVector().getNormalized();
	return (getCurrLengthVector() - dir * defaultLength) * k;
}

float Spring::getCurrLength(void)
{
	return startPos->calcDistance(*endPos);
}

Vector3f Spring::getCurrLengthVector(void)
{
	return *startPos - *endPos;
}

void Spring::draw(void)
{
	glBegin(GL_LINES);
		glVertex3fv(&startPos->x);
		glVertex3fv(&endPos->x);
	glEnd();
}
