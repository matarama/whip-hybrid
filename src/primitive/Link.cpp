
#include <stdlib.h>
#include <stdio.h>
#include <GL/glut.h>
#include <math.h>
#include <iostream>

#include "include/Config.h"
#include "include/primitive/Common.h"

#include "include/primitive/Link.h"

Link::Link(void)
: head(NULL),
  tail(NULL),
  length(0.0f),
  dampingConstant(0.0f)
{
}

Link::Link(PointMass* head, PointMass* tail, float length, float dampingConstant)
: head(head),
  tail(tail),
  length(length),
  dampingConstant(dampingConstant)
{
}

Link::~Link(void)
{

}

void Link::followThrough(void)
{
	Vector3f dir = (tail->position - head->position).getNormalized();
	tail->position = head->position + dir * length;
}

void Link::update(int timeSlice)
{
	// update the velocity of tail point with its current forces
	tail->update(timeSlice);

	// Calculate the intervention vector perpendicular to the link
	tail->velocity = calculateValidVector(tail->velocity);

	// Direction of angular velocity is from outer point to center
	Vector3f dir = (tail->position - head->position).getNormalized();

	// Calculate angular velocity using formula W = w x r
	float magn = tail->velocity.getLength() / length;

	// accumulate
	angularVelocity += dir * magn;
}

void Link::rotate(int timeSlice)
{
	// Calculate the angle that this link will be rotated by
	float angle = degreeToRadian(timeSlice * angularVelocity.getLength());

	// Calculate rotation axis
	Vector3f linkVecDir = (tail->position - head->position).getNormalized();
	Vector3f velVecDir = tail->velocity.getNormalized();
	Vector3f rotationAxis = linkVecDir.crossProd(velVecDir).getNormalized();

	// Calculate tail's positions after rotating the link by head
	tail->position = tail->position.rotateAbout(rotationAxis, angle);

	// Apply damping force
	angularVelocity *= dampingConstant;
	tail->velocity.reset();
}

void Link::draw(void)
{
	glBegin(GL_LINES);
		glVertex3fv(&head->position.x);
		glVertex3fv(&tail->position.x);
	glEnd();
}


// Private Functions
// -------------------------------------------------------------------------------------------------------

Vector3f Link::calculateValidVector(Vector3f intervention)
{
	// Find the direction vector of this link (from end to start position)
	Vector3f linkDir = (head->position - tail->position).getNormalized();

	// Calculate the angle between link-vector and outside intervention (force, vector, acceleration...)
	float angle = linkDir.getAngle(intervention);

	// angle is 90 degrees, intervention is linearly independent of link orientation, so just return it
	if(abs(angle - M_PI / 2) < ERR)
		return intervention;

	// Find a 3rd vector that is perpendicular to both link vector and intervention.
	// This will form the axis by which we will rotate intervention.
	Vector3f normal = linkDir.crossProd(intervention).getNormalized();

	// After subtracting intervention vectors linearly dependent part on link-vector
	// It is magnitude will be validMagn
	float thetaRad = M_PI - angle;
	float validMagn = intervention.getLength() * sin(thetaRad);

	// calculate the direction of intervention vector and rotate it thetaRad degrees around
	// linearly independent axis we calculated above
	Vector3f validDir = intervention.getNormalized();
	validDir = validDir.rotateAbout(normal, -thetaRad);

	// Adjust direction again
	if(angle < M_PI / 2)
		validDir = -validDir;

	// validDir is the direction perpendicular to link-vector
	return validDir * validMagn;
}
