
#include <math.h>

#include "include/primitive/Common.h"

float radianToDegree(float radian)
{
	return (radian / M_PI) * 180.0f;
}

float degreeToRadian(float degree)
{
	return (degree / 180.0f) * M_PI;
}
