
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <GL/glut.h>

#include "include/Config.h"

#include "include/primitive/PointMass.h"

PointMass::PointMass(void)
: m(0.0f),
  dampingConstant(1.0f),
  fixed(false)
{

}

PointMass::PointMass(float m, float dampingConstant, Vector3f position, bool fixed)
: m(m),
  dampingConstant(dampingConstant),
  position(position),
  fixed(fixed)
{
}

void PointMass::addForce(Vector3f f)
{
	if(fixed)
		return;

	force += f;
}

void PointMass::move(Vector3f newPosition)
{
	position += newPosition;
}

void PointMass::move(int timeSlice)
{
	position += velocity * timeSlice;
	velocity *= dampingConstant;
}

void PointMass::update(int timeSlice)
{
	Vector3f acc = force / m;
	velocity += acc * timeSlice;

	if(velocity.getLength() < MIN_VELOCITY)
		velocity.reset();

	force.reset();
}

void PointMass::attachHead(Spring* spring)
{
	attachedSpringsHead.push_back(spring);
}

void PointMass::attachTail(Spring* spring)
{
	attachedSpringsTail.push_back(spring);
}

void PointMass::draw(void)
{
	glBegin(GL_LINES);
		glVertex3f(position.x - 0.2, position.y, position.z);
		glVertex3f(position.x + 0.2, position.y, position.z);
		glVertex3f(position.x, position.y - 0.2, position.z);
		glVertex3f(position.x, position.y + 0.2, position.z);
	glEnd();
}
