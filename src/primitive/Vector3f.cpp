
#include <stdlib.h>
#include <stdio.h>
#include <sstream>
#include <math.h>

#include "include/primitive/Matrix4f.h"

#include "include/primitive/Vector3f.h"

#define vector3f_calc_scalar(op, value) \
	return Vector3f(x op value, y op value, z op value);

#define vector3f_calc_vector(op, vec) \
	return Vector3f(x op vec.x, y op vec.y, z op vec.z);

#define vector3f_calc_scalar_self(op, value) \
	{ \
		x = x op value; \
		y = y op value; \
		z = z op value; \
		return *this; \
	}

#define vector3f_calc_vector_self(op, vec) \
	{ \
		x = x op vec.x; \
		y = y op vec.y; \
		z = z op vec.z; \
		return *this; \
	}

Vector3f::Vector3f(void)
: x(0.0),
  y(0.0),
  z(0.0)
{
}

Vector3f::Vector3f(float x, float y, float z)
: x(x),
  y(y),
  z(z)
{
}

Vector3f::Vector3f(const Vector3f& other)
: x(other.x),
  y(other.y),
  z(other.z)
{
}

Vector3f& Vector3f::operator=(const Vector3f& other)
{
	x = other.x;
	y = other.y;
	z = other.z;

	return *this;
}

Vector3f::~Vector3f(void)
{
}

float Vector3f::calcDistanceSquared(Vector3f other)
{
	return (x - other.x) * (x - other.x) + (y - other.y) * (y - other.y) + (z - other.z) * (z - other.z);
}

float Vector3f::calcDistance(Vector3f other)
{
	return sqrt(calcDistanceSquared(other));
}

float Vector3f::getLengthSquared(void)
{
	return x * x + y * y + z * z;
}

float Vector3f::getLength(void)
{
	return sqrt(getLengthSquared());
}

void Vector3f::normalize(void)
{
	float len = getLength();
	if(len == 0)
		return;

	x /= len;
	y /= len;
	z /= len;
}

Vector3f Vector3f::getNormalized(void)
{
	Vector3f vec(*this);
	vec.normalize();

	return vec;
}

std::string Vector3f::toString(void)
{
	std::stringstream converter;
	converter << *this;

	return converter.str();
}

float Vector3f::dotProd(Vector3f v)
{
	return this->x * v.x + this->y * v.y + this->z * v.z;
}

Vector3f Vector3f::crossProd(Vector3f v)
{
	return Vector3f(this->y * v.z - this->z * v.y,
					this->z * v.x - this->x * v.z,
					this->x * v.y - this->y * v.x);
}

float Vector3f::getAngle(Vector3f v)
{
	float dp = dotProd(v);
	float v1Len = getLength();
	float v2Len = v.getLength();

	return acos(dp / (v1Len * v2Len));
}

void Vector3f::reset(void)
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
}

Vector3f Vector3f::operator+(float scalar) { vector3f_calc_scalar(+, scalar) }
Vector3f Vector3f::operator-(float scalar) { vector3f_calc_scalar(-, scalar) }
Vector3f Vector3f::operator/(float scalar) { vector3f_calc_scalar(/, scalar) }
Vector3f Vector3f::operator*(float scalar) { vector3f_calc_scalar(*, scalar) }
Vector3f Vector3f::operator+(Vector3f other) { vector3f_calc_vector(+, other) }
Vector3f Vector3f::operator-(Vector3f other) { vector3f_calc_vector(-, other) }
Vector3f Vector3f::operator/(Vector3f other) { vector3f_calc_vector(/, other) }
Vector3f Vector3f::operator*(Vector3f other) { vector3f_calc_vector(*, other) }

Vector3f& Vector3f::operator+=(float scalar) { vector3f_calc_scalar_self(+, scalar) }
Vector3f& Vector3f::operator-=(float scalar) { vector3f_calc_scalar_self(-, scalar) }
Vector3f& Vector3f::operator/=(float scalar) { vector3f_calc_scalar_self(/, scalar) }
Vector3f& Vector3f::operator*=(float scalar) { vector3f_calc_scalar_self(*, scalar) }
Vector3f& Vector3f::operator+=(Vector3f other) { vector3f_calc_vector_self(+, other) }
Vector3f& Vector3f::operator-=(Vector3f other) { vector3f_calc_vector_self(-, other) }
Vector3f& Vector3f::operator/=(Vector3f other) { vector3f_calc_vector_self(/, other) }
Vector3f& Vector3f::operator*=(Vector3f other) { vector3f_calc_vector_self(*, other) }

bool Vector3f::operator==(Vector3f other)
{
	return x == other.x && y == other.y && z == other.z;
}

Vector3f Vector3f::operator-() const
{
	return Vector3f(-x, -y, -z);
}

bool Vector3f::hasOppositeDir(Vector3f other)
{
	Vector3f myReverseDir = getNormalized() * -1;
	Vector3f otherDir = other.getNormalized();

	return myReverseDir == otherDir;
}

bool Vector3f::hasSameDir(Vector3f other)
{
	Vector3f myDir = getNormalized();
	Vector3f otherDir = other.getNormalized();

	return myDir == otherDir;
}

Vector3f Vector3f::rotateAbout(Vector3f axis, float angle)
{
	// Rx(@x)
	Matrix4f Rxt;
	Rxt(0, 0) = 1.0f;
	Rxt(1, 1) = axis.z; Rxt(1, 2) = -axis.y;
	Rxt(2, 2) = axis.y; Rxt(2, 3) = axis.z;
	Rxt(3, 3) = 1.0f;

	// Rx(-@x)
	Matrix4f Rx_t;
	Rx_t(0, 0) = 1.0f;
	Rx_t(1, 1) = axis.z;	Rx_t(1, 2) = axis.y;
	Rx_t(2, 2) = -axis.y;	Rx_t(2, 3) = axis.z;
	Rx_t(3, 3) = 1.0f;

	// Ry(@y)
	Matrix4f Ryt;
	Ryt(0, 0) = 1.0f; Ryt(0, 2) = -axis.x;
	Ryt(1, 1) = 1.0f;
	Ryt(2, 0) = axis.x; Ryt(2, 2) = 1.0f;
	Ryt(3, 3) = 1.0f;

	// Ry(-@y)
	Matrix4f Ry_t;
	Ry_t(0, 0) = 1.0f; Ry_t(0, 2) = axis.x;
	Ry_t(1, 1) = 1.0f;
	Ry_t(2, 0) = -axis.x; Ry_t(2, 2) = 1.0f;
	Ry_t(3, 3) = 1.0f;

	// Rz(angle)
	Matrix4f Rz;
	Rz(0, 0) = cos(angle); Rz(0, 1) = -sin(angle);
	Rz(1, 0) = sin(angle); Rz(1, 1) = cos(angle);
	Rz(2, 2) = 1.0f;
	Rz(3, 3) = 1.0f;

	// R = Rx(-@x) Ry(-@y) Rz(angle) Ry(@y) Rx(@x)
	// Matrix4f R = Rxt * Ryt * Rz * Ry_t * Rx_t;
	Matrix4f R = Rxt * Ryt * Rz * Ry_t * Rx_t;

	// return Rv
	return R * (*this);
}
