
#include "include/primitive/Matrix4f.h"

// Some macros for readability
// ------------------------------------------------------------------------------------

#define matrix4f_calc_scalar(op, scalar) 	\
	Matrix4f res;							\
	for(int i = 0; i < 16; ++i) 			\
		res.data[i] = data[i] op scalar; 	\
	return res;

#define matrix4f_calc_scalar_self(op, scalar)	\
	for(int i = 0; i < 16; ++i) 				\
		data[i] = data[i] op scalar; 			\
	return *this;

#define matrix4f_calc_matrix(op, other)			\
	Matrix4f res;								\
	for(int i = 0; i < 16; ++i)					\
		res.data[i] = data[i] op other.data[i];	\
	return res;

#define matrix4f_calc_matrix_self(op, other)	\
	for(int i = 0; i < 16; ++i)					\
		data[i] = data[i] op other.data[i];		\
	return *this;

// ------------------------------------------------------------------------------------

Matrix4f::Matrix4f(void)
{
	init();
}

Matrix4f::Matrix4f(float* data)
{
	copy(data);
}

Matrix4f::Matrix4f(const Matrix4f& other)
{
	copyOther(other);
}

Matrix4f& Matrix4f::operator=(const Matrix4f& other)
{
	if(this != &other)
		copyOther(other);

	return *this;
}

Matrix4f::~Matrix4f(void)
{
}

float Matrix4f::operator()(unsigned int i, unsigned int j) const
{
	return data[i * 4 + j];
}

float& Matrix4f::operator()(unsigned int i, unsigned int j)
{
	return data[i * 4 + j];
}

Matrix4f Matrix4f::operator+(float scalar) { matrix4f_calc_scalar(+, scalar) }
Matrix4f Matrix4f::operator-(float scalar) { matrix4f_calc_scalar(-, scalar) }
Matrix4f Matrix4f::operator/(float scalar) { matrix4f_calc_scalar(/, scalar) }
Matrix4f Matrix4f::operator*(float scalar) { matrix4f_calc_scalar(*, scalar) }
Matrix4f Matrix4f::operator+(Matrix4f other) { matrix4f_calc_matrix(+, other) }
Matrix4f Matrix4f::operator-(Matrix4f other) { matrix4f_calc_matrix(-, other) }
Matrix4f Matrix4f::operator/(Matrix4f other) { matrix4f_calc_matrix(/, other) }
Matrix4f Matrix4f::operator*(Matrix4f other)
{
	Matrix4f res;
	for(int i = 0; i < 4; ++i)
		for(int j = 0; j < 4; ++j)
			for(int k = 0; k < 4; ++k)
				res(i, j) += (*this)(i, k) * other(k, j);
	return res;
}

Matrix4f& Matrix4f::operator+=(float scalar) { matrix4f_calc_scalar_self(+, scalar) }
Matrix4f& Matrix4f::operator-=(float scalar) { matrix4f_calc_scalar_self(-, scalar) }
Matrix4f& Matrix4f::operator/=(float scalar) { matrix4f_calc_scalar_self(/, scalar) }
Matrix4f& Matrix4f::operator*=(float scalar) { matrix4f_calc_scalar_self(*, scalar) }
Matrix4f& Matrix4f::operator+=(Matrix4f other) { matrix4f_calc_matrix_self(+, other) }
Matrix4f& Matrix4f::operator-=(Matrix4f other) { matrix4f_calc_matrix_self(-, other) }
Matrix4f& Matrix4f::operator/=(Matrix4f other) { matrix4f_calc_matrix_self(/, other) }

Vector3f Matrix4f::operator*(Vector3f v)
{
	Vector3f result;
	result.x = (*this)(0, 0) * v.x + (*this)(0, 1) * v.y + (*this)(0, 2) * v.z;
	result.y = (*this)(1, 0) * v.x + (*this)(1, 1) * v.y + (*this)(1, 2) * v.z;
	result.z = (*this)(2, 0) * v.x + (*this)(2, 1) * v.y + (*this)(2, 2) * v.z;

	return result;
}

// Private functions
// ----------------------------------------------------------------------------------

void Matrix4f::copyOther(const Matrix4f& other)
{
	for(int i = 0; i < 16; ++i)
		this->data[i] = other.data[i];
}

void Matrix4f::copy(float* data)
{
	for(int i = 0; i < 16; ++i)
		this->data[i] = data[i];
}

void Matrix4f::init(void)
{
	for(int i = 0; i < 16; ++i)
		this->data[i] = 0.0f;
}
