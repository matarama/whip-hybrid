
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <stdarg.h>
#include <GL/glut.h>
#include <pthread.h>
#include <iostream>
using namespace std;

#include "include/primitive/Link.h"
#include "include/primitive/Common.h"
#include "include/primitive/Vector3f.h"
#include "include/primitive/Matrix4f.h"
#include "include/data_structure/Whip.h"

// FPS parameters
// ---------------------------------------------------------------------------

unsigned int currentTime;
unsigned int previousTime;
unsigned int remainingTime;

// time-step is 16 miliseconds which makes simulation run at 62.5 fps
unsigned int timeStep = 16;

// Animation Parameters
// ---------------------------------------------------------------------------

int windowHeight = 600;
int windowWidth = 800;

// Simulation parameters
// ---------------------------------------------------------------------------

bool gravityOn = false;
Whip dynamo(8, 1, 1);

// ---------------------------------------------------------------------------

extern void reshape(int width, int height);
extern void display(void);
extern void idle(void);
extern void mouse(int btn, int state, int x, int y);
extern void keyboard(unsigned char c, int x, int y);
/*
int main(void)
{
	Matrix4f mtx;
	mtx(0,0) = 0; mtx(0,1) = 1; mtx(0,2) = 2; mtx(0,3) = 3;
	mtx(1,0) = 4; mtx(1,1) = 5; mtx(1,2) = 6; mtx(1,3) = 7;
	mtx(2,0) = 8; mtx(2,1) = 9; mtx(2,2) = 10; mtx(2,3) = 11;
	mtx(3,0) = 12; mtx(3,1) = 13; mtx(3,2) = 14; mtx(3,3) = 15;

	Matrix4f m2;
	m2(1, 1) = 3;
	m2(2, 3) = 1;

	cout << mtx << endl;
	cout << m2 << endl;
	cout << mtx * m2 << endl;
	cout << m2 - mtx << endl;
	cout << mtx - m2 << endl;

	Matrix4f m3 = m2;
	m3 += 4;
	cout << m3 << endl;

	Matrix4f m4;
	m4(0,0) = 1; m4(0,1) = 1; m4(0,2) = 1; m4(0,3) = 1;
	m4(1,0) = 1; m4(1,1) = 1; m4(1,2) = 1; m4(1,3) = 1;
	m4(2,0) = 1; m4(2,1) = 1; m4(2,2) = 1; m4(2,3) = 1;
	m4(3,0) = 1; m4(3,1) = 1; m4(3,2) = 1; m4(3,3) = 1;
	Vector3f v(1, 2, 3);

	Vector3f v2 = m4 * v;

	cout << v2 << endl;

	return EXIT_SUCCESS;
}
*/

/*
Vector3f rotate(Vector3f v, Vector3f axisDir, float angle)
{
	printf("angle: %0.2f\n", radianToDegree(angle));

	Vector3f res;

	// Rx(@x)
	Matrix4f Rxt;
	Rxt(0, 0) = 1.0f;
	Rxt(1, 1) = axisDir.z; Rxt(1, 2) = -axisDir.y;
	Rxt(2, 2) = axisDir.y; Rxt(2, 3) = axisDir.z;
	Rxt(3, 3) = 1.0f;

	// Rx(-@x)
	Matrix4f Rx_t;
	Rx_t(0, 0) = 1.0f;
	Rx_t(1, 1) = axisDir.z;	Rx_t(1, 2) = axisDir.y;
	Rx_t(2, 2) = -axisDir.y;	Rx_t(2, 3) = axisDir.z;
	Rx_t(3, 3) = 1.0f;

	// Ry(@y)
	Matrix4f Ryt;
	Ryt(0, 0) = 1.0f; Ryt(0, 2) = -axisDir.x;
	Ryt(1, 1) = 1.0f;
	Ryt(2, 0) = axisDir.x; Ryt(2, 2) = 1.0f;
	Ryt(3, 3) = 1.0f;

	// Ry(-@y)
	Matrix4f Ry_t;
	Ry_t(0, 0) = 1.0f; Ry_t(0, 2) = axisDir.x;
	Ry_t(1, 1) = 1.0f;
	Ry_t(2, 0) = -axisDir.x; Ry_t(2, 2) = 1.0f;
	Ry_t(3, 3) = 1.0f;

	// Rz(angle)
	Matrix4f Rz;
	Rz(0, 0) = cos(angle); Rz(0, 1) = -sin(angle);
	Rz(1, 0) = sin(angle); Rz(1, 1) = cos(angle);
	Rz(2, 2) = 1.0f;
	Rz(3, 3) = 1.0f;

	// R = Rx(-@x) Ry(-@y) Rz(angle) Ry(@y) Rx(@x)
	// Matrix4f R = Rxt * Ryt * Rz * Ry_t * Rx_t;
	Matrix4f R = Rxt * Ryt * Rz * Ry_t * Rx_t;

	res = R * v;

	return res;
}
*/
/*
int main(void)
{
	Vector3f startPos(0, 1, 0);
	Vector3f endPos(0, 0, 0);

	Link link(&startPos, &endPos, 1);
	Vector3f external(0, -10, 0);

	Vector3f validExt = link.calculateValidVector(external);

	cout << link << endl;
	cout << "external vector: " << external << endl;
	cout << "valid vector: " << validExt << endl;

	return EXIT_SUCCESS;
}
*/
/*
int main(void)
{
	Vector3f f1(0, -1, 0);
	Vector3f f2(0, -10, 0);

	cout << "f1: " << f1 << endl;
	cout << "f2: " << f2 << endl;

	float theta1 = f1.getAngle(f2);
	float theta2 = f2.getAngle(f1);

	printf("RADIAN: theta1: %0.2f theta2: %0.2f\n", theta1, theta2);
	printf("DEGREE: theta1: %0.2f theta2: %0.2f\n", radianToDegree(theta1), radianToDegree(theta2));

	Vector3f normal1 = f1.crossProd(f2);
	Vector3f normal2 = f2.crossProd(f1);

	cout << "Normal1: " << normal1 << endl;
	cout << "Normal2: " << normal2 << endl;

	float theta = M_PI - theta1;
	printf("cos(%0.2f)=%0.2f\n", radianToDegree(theta), cos(theta));


	float magn= f2.getLength() * sin(theta);

	cout << "Magn: " << magn << endl;


	Vector3f normal = f1.crossProd(f2);

	Vector3f rotated = rotate(f2.getNormalized(), normal.getNormalized(), -theta) * magn;
	if(theta1 < M_PI / 2)
		rotated = rotated * -1;

	cout << "rotated: " << rotated << endl;

	cout << "Degree: " << rotated.getAngle(f1) << "\n";

	return EXIT_SUCCESS;
}
*/

int main(int argc, char* argv[])
{
	glutInit(&argc, argv);
	previousTime = glutGet(GLUT_ELAPSED_TIME);
	remainingTime = 0;

	int screenWidth = glutGet(GLUT_SCREEN_WIDTH);
	int heightWidth = glutGet(GLUT_SCREEN_WIDTH);

	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutInitWindowPosition(screenWidth - windowWidth, 0);
	glutInitWindowSize(windowWidth, windowHeight);
	glutCreateWindow("Dynamo");
	glutReshapeFunc(reshape);
	glutDisplayFunc(display);
	glutIdleFunc(idle);
	glutMouseFunc(mouse);
	glutKeyboardFunc(keyboard);
	glEnable(GL_DEPTH_TEST);

	glutMainLoop();

	return EXIT_SUCCESS;
}

void reshape(int width, int height)
{
	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if(width <= height)
		glOrtho(-10.0, 10.0, -10.0 * (GLfloat) height / (GLfloat) width, 10.0 * (GLfloat) height / (GLfloat) width, -10.0, 10.0);
	else
		glOrtho(-10.0 * (GLfloat) width / (GLfloat) height, 10.0 * (GLfloat) width / (GLfloat) height, -10.0, 10.0, -10.0, 10.0);

	glMatrixMode(GL_MODELVIEW);

}

void display(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	dynamo.draw();

	glFlush();
	glutSwapBuffers();
}

void idle(void)
{
	currentTime = glutGet(GLUT_ELAPSED_TIME);
	remainingTime += currentTime - previousTime;

	if(remainingTime >= 16)
	{
		remainingTime -= 16;
	}
	// Limit FPS at 60
	// limitFPS();

	previousTime = currentTime;
}

void mouse(int btn, int state, int x, int y)
{

}

void keyboard(unsigned char c, int x, int y)
{
	if(c == 'g')
	{
		gravityOn = !gravityOn;
		printf("gravity-on: %d\n", gravityOn);
	}
	else if(c == 'f')
	{
		dynamo.applyExternal(Vector3f(0.0, 0.1, 0.0));
		printf("Velocity applied!!\n");
	}
	else if(c == 's')
	{
		printf("Simulating one step.\n");
		if(gravityOn)
			dynamo.applyInternal(Vector3f(0.0, -0.0098, 0.0));
		dynamo.simulate();
	}
	else if(c == 'r')
	{
		dynamo.links[0].rotate(16);
	}
	else if(c == ' ')
	{
		std::cout << "DYNAMO\n" << dynamo;
	}
	else
	{
		printf("btn: %c\n", c);
	}

	glutPostRedisplay();
}
