#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <string>
#include <math.h>
#include <GL/glut.h>

#include "include/primitive/Common.h"
#include "include/primitive/Spring.h"
#include "include/primitive/Link.h"

#include "include/data_structure/Whip.h"

Whip::Whip(void)
: timeSlice(0),
  pointMassCount(0),
  pointMasses(NULL),
  linkCount(0),
  links(NULL),
  springCount(0),
  springs(NULL)
{
}

Whip::Whip(int pointMassCount, float offset, float mass)
: timeSlice(16),
  pointMassCount(pointMassCount),
  pointMasses(NULL),
  linkCount(pointMassCount - 1),
  links(NULL),
  springCount(linkCount - 1),
  springs(NULL)
{
	pointMasses = new PointMass[pointMassCount];
	for(int i = 0; i < pointMassCount; ++i)
		pointMasses[i] = PointMass(mass, 0.97f, Vector3f(offset * i, 0.0f, 0.0f));

	pointMasses[0].fixed = true;

	links = new Link[linkCount];
	for(int i = 0; i < linkCount; ++i)
		links[i] = Link(&pointMasses[i], &pointMasses[i + 1], offset, 0.98 - i * 0.02);


	springs = new Spring[springCount];
	for(int i = 0; i < springCount; ++i)
	{
		PointMass* headMass = &pointMasses[0];
		PointMass* tailMass = &pointMasses[i + 2];

		springs[i] = Spring(i, (springCount * springCount - i * i), &headMass->position, &tailMass->position);
		headMass->attachHead(&springs[i]);
		tailMass->attachHead(&springs[i]);
	}
}

Whip::~Whip(void)
{
	delete[] pointMasses;
	delete[] links;
	delete[] springs;
}

void Whip::simulate(void)
{
	pointMasses[0].update(timeSlice);
	pointMasses[0].move(timeSlice);

	for(int i = 0; i < linkCount; ++i)
	{
	//	links[i].followThrough();
		links[i].update(timeSlice);
		//links[i].followThrough();
		links[i].rotate(timeSlice);
		links[i].followThrough();
	}
}

void Whip::applyExternal(Vector3f f)
{
	// apply force only to first point-mass (handle of the whip)
	pointMasses[0].addForce(f);
}

void Whip::applyInternal(Vector3f f)
{
	for(int i = 0; i < pointMassCount; ++i)
		pointMasses[i].addForce(f);
}

void Whip::draw(void)
{
	for(int i = 0; i < linkCount; ++i)
		links[i].draw();

	glColor3f(1.0f, 0.0f, 0.0f);
	pointMasses[0].draw();
	glColor3f(0.0f, 1.0f, 0.0f);
	for(int i = 0; i < pointMassCount; ++i)
		pointMasses[i].draw();
	glColor3f(1.0f, 1.0f, 1.0f);
}

// private functions
// --------------------------------------------------------------------------
