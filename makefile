
BUILD_DIR = build
SRC = src
INC = include
TEST = test
IK = ik
PRM = primitive
DS = data_structure
MA = main

INC_PATHS = -I/usr/local/include -I/usr/include -I/home/matara/workspace/WhipHybrid/src
LIB_PATHS = -L/usr/local/lib -L/usr/local/lib64 -L/usr/X11R6/lib

MAIN_FILE = $(RUN_MODE)

CC = g++
CFLAGS = -c -g3 $(INC_PATHS)

LD = g++
LIBS = -lGL -lGLU -lglut -lpthread 
LDFLAGS =  $(LIB_PATHS) $(LIBS)

HEADERS = 
FILES = $(MA)/$(MAIN_FILE) $(DS)/Whip $(PRM)/PointMass $(PRM)/Link $(PRM)/Spring $(PRM)/Vector3f $(PRM)/Matrix4f $(PRM)/Common
		
OBJS = $(foreach file, $(FILES), $(BUILD_DIR)/$(file).o)

all : clean_up $(BUILD_DIR)/$(MAIN_FILE)
	@echo ""
	@echo "Build successful!!"

clean : rm -rf $(OBJS)

clean_up:
	rm -rf $(OBJS)

################################################################################################	

$(BUILD_DIR)/$(MAIN_FILE) : $(OBJS) 
	$(LD) $(OBJS) $(LDFLAGS) -o $(BUILD_DIR)/$(MAIN_FILE)

# Run Mode
################################################################################################

$(BUILD_DIR)/$(MA)/$(MAIN_FILE).o : $(SRC)/$(MA)/$(MAIN_FILE).cpp
	$(CC) $(CFLAGS) $(SRC)/$(MA)/$(MAIN_FILE).cpp -o $(BUILD_DIR)/$(MA)/$(MAIN_FILE).o

# Data Structures
################################################################################################

$(BUILD_DIR)/$(DS)/Whip.o : $(SRC)/$(DS)/Whip.cpp $(SRC)/$(INC)/$(DS)/Whip.h
	$(CC) $(CFLAGS) $(SRC)/$(DS)/Whip.cpp -o $(BUILD_DIR)/$(DS)/Whip.o	
	
# Primitives
################################################################################################

$(BUILD_DIR)/$(PRM)/Vector3f.o : $(SRC)/$(PRM)/Vector3f.cpp $(SRC)/$(INC)/$(PRM)/Vector3f.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/Vector3f.cpp -o $(BUILD_DIR)/$(PRM)/Vector3f.o	

$(BUILD_DIR)/$(PRM)/Matrix4f.o : $(SRC)/$(PRM)/Matrix4f.cpp $(SRC)/$(INC)/$(PRM)/Matrix4f.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/Matrix4f.cpp -o $(BUILD_DIR)/$(PRM)/Matrix4f.o
	
$(BUILD_DIR)/$(PRM)/Common.o : $(SRC)/$(PRM)/Common.cpp $(SRC)/$(INC)/$(PRM)/Common.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/Common.cpp -o $(BUILD_DIR)/$(PRM)/Common.o
	
$(BUILD_DIR)/$(PRM)/Link.o : $(SRC)/$(PRM)/Link.cpp $(SRC)/$(INC)/$(PRM)/Link.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/Link.cpp -o $(BUILD_DIR)/$(PRM)/Link.o
	
$(BUILD_DIR)/$(PRM)/Spring.o : $(SRC)/$(PRM)/Spring.cpp $(SRC)/$(INC)/$(PRM)/Spring.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/Spring.cpp -o $(BUILD_DIR)/$(PRM)/Spring.o

$(BUILD_DIR)/$(PRM)/PointMass.o : $(SRC)/$(PRM)/PointMass.cpp $(SRC)/$(INC)/$(PRM)/PointMass.h
	$(CC) $(CFLAGS) $(SRC)/$(PRM)/PointMass.cpp -o $(BUILD_DIR)/$(PRM)/PointMass.o
